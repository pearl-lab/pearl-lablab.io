---
layout: page
title: Team  Members
permalink: /team/
---

[Xiangchen Li](https://www.linkedin.com/in/xiangchen-li-aba77825b/) Ph.D. Researcher     
[Emad Mazied](https://www.linkedin.com/in/emadeldin-mazied-abdrabou-115b19120/) Ph.D. Researcher   
[Shunyu Yao](https://www.linkedin.com/in/riyos-p-607157253/) (also with [DSSL](https://dssl.cs.vt.edu/)) Ph.D. Researcher   
[Max Fisher](https://www.linkedin.com/in/mhfisher/), M.Sc. Researcher
[Tim Coyne](https://www.linkedin.com/in/timothy-coyne-556539295/), M.Sc. Researcher      
[Alex Lin](https://www.linkedin.com/in/alxn3/) Graduate Researcher       
[Jack Williamson](https://www.linkedin.com/in/jack-williamson1/) Graduate Researcher   
[Anthony Nguyen](https://www.linkedin.com/in/ant33nguyen/) Undergraduate Researcher      
[Gianfranco Vivanco](https://www.linkedin.com/in/gianfranco-vivanco-477a36280/) Undergraduate Researcher   
