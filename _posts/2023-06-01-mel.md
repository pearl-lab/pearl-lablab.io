---
layout:	post
title:	"Mel Cameron successfully defended her M.Sc. thesis"
date:	2023-06-01
categories: pearl graduations 
---

Congratulations to Mel Cameron who successfully defended her M.Sc. thesis! Mel's thesis was on a diversity aware tool for parallel computing education called Parallel Islands. Mel's next step in her career is a teaching position at the [Virginia Tech Innovation Campus](https://www.vt.edu/innovationcampus/index.html).





