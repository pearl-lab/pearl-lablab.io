---
layout:	post
title:	"New NSF Award on Wearable Intelligence"
date:	2023-10-01
categories: pearl newstudents 
---

We received a new [NSF award](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2315851&HistoricalAwards=false), to work on  wearable intelligence in healthcare applications. This is a fantastic international collaboration with [Bo Ji](https://people.cs.vt.edu/boji/), [Hans Vandierendonck](https://pure.qub.ac.uk/en/persons/hans-vandierendonck), and [Deepu John](https://people.ucd.ie/deepu.john). Keep an eye on this space!  
