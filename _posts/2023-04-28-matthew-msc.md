---
layout:	post
title:	"Matthew Jackson successfully defended his M.Sc. thesis"
date:	2023-04-28
categories: pearl graduations 
---

Congratulations to Matthew Jackson who successfully defended his M.Sc. thesis! Matthew's thesis was on computational offloading for real-time computer vision in unreliable multi-tenant edge systems. Matthew will join Johns Hopkins University later this summer.





