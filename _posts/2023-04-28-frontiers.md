---
layout:	post
title:	"Paper on Autoscaling Edge Cloud for Network Slicing"
date:	2023-04-28
categories: pearl papers 
---

Emad Mazied's  paper on vertical autoscaling of edge cloud resources for radio workloads was accepted in [Frontiers in High Performance Computing](https://www.frontiersin.org/journals/high-performance-computing). The presents a study on resource control for autoscaling virtual radio access networks (RAN slices) in next-generation wireless networks. This is joint work with [PEARL](https://research.cs.vt.edu/~pearl) Director [Dimitrios S. Nikolopoulos](https://cs.vt.edu/~dsn) and collaborators Yasser Hanafy and Scott Midkiff. 




