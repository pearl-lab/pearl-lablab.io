---
layout:	post
title:	"Dimitrios named IEEE Fellow"
date:	2023-11-22
categories: pearl awards
---

Dimitrios S. Nikolopoulos, PEARL Director, was named [IEEE Fellow](https://www.ieee.org/membership/fellows/index.html) for contributions to dynamic program execution environments and multiprocessor memory management.