---
layout:	post
title:	"Paper accepted at IPDPS 2024"
date:	2023-12-11
categories: pearl publications 
---

Congratulations to all co-authors (Moiz Arif, Avinash Maruya, Mustafa Rafique, and Ali R. Butt), for a paper on Application-Attuned Memory Management for Containerized HPC Workflows, accepted at [IPDPS 2024](https://www.ipdps.org/). We are rethinking memory management in light of disaggregation and unique aspects of serverless computing workloads.


