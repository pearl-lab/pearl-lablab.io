---
layout:	post
title:	"Welcome to our new PhD Xiangchen Li"
date:	2023-10-01
categories: pearl newstudents 
---

We are delighted to welcome [Xiangchen Li](https://www.linkedin.com/in/xiangchen-li-aba77825b) to our lab. Xiangchen will work on the NSF SWEET project, specifically on dynamic partitioning and quantizing ML models for edge devices and networks.


