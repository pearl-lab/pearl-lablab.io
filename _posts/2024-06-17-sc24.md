---
layout:	post
title:	"Paper accepted at SC2024"
date:	2023-06-17
categories: pearl publications 
---

Congratulations to all co-authors (Munkyu Lee, Sihoon Seong, Minki Kang, Jihyuk Lee, Gap-Joo Na, In-Geol Chun, Cheol-Ho Hong and Dimitrios S. Nikolopoulos), for a paper on ParvaGPU: Efficient Spatial GPU Sharing for Large-Scale DNN Inference in Cloud Environments, accepted at [SC2024](https://sc24.supercomputing.org/). We present 
rigorous algorithms and a system for reducing the GPU footprint of large-scale inference worklaods.


