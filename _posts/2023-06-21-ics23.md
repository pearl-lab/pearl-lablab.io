---
layout:	post
title:	"Dimitrios chairs the program of the 37th ACM International Conference on Supercomputing"
date:	2023-06-14
categories: pearl conferences  
---

[Dimitrios Nikolopoulos](https://www.cs.vt.edu/~dsn) served as Program Chair of the [37th ACM International Conference on Supercomputing](https://nschiele.github.io/ICS2023/program.html), which was held June 21-23 as part of [ACM FCRC](https://fcrc.acm.org/). The conference brought together leading researchers in the architecture, software, and applications of high-performance computing systems. The program featured an excellent keynote by [Bill Magro](https://www.linkedin.com/in/william-magro) on the future of high-performance computing in a highly heterogeneous but also converged hardware ecosystem. Dimitrios also served as keynote chair of the [14th ACM Conference on e-Energy](https://energy.acm.org/conferences/eenergy/2023/).

![ICS23 photos](/assets/ics23.png)



