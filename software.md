---
layout: page
title: Software
permalink: /software/
---

[Streamflow: Locality-Aware, NUMA-Aware, Lock-Free Multithreaded Memory Allocator](https://github.com/scotts/streamflow)  
[Swan: Dataflow-Driven Work-Stealing Scheduler](https://github.com/hvdieren/swan)(collaboration  with [DSSC](https://www.qub.ac.uk/ecit/DSSC/) at QUB)  
[GraphGrind: Improved Locality Graph Partitioning](https://github.com/hvdieren/GraphGrind-ICPP)  (collaboration with [DSSC](https://www.qub.ac.uk/ecit/DSSC/) at QUB)  
[SAFIRE Compiler-Driven Parallel Fault Injection Tool](https://github.com/LLNL/SAFIRE) (collaboration with LLNL)  
[Dynamic Precision Iterative Refinement](https://github.com/oprecomp/DynIR) (collaboration with [DSSC](https://www.qub.ac.uk/ecit/DSSC/) at QUB)  
[Serverless Deep Learning Framework](https://github.com/MoizArif/dl-serverless) (collaboration with [HPDSL](https://www.cs.rit.edu/~hpdsl/) at RIT)    


