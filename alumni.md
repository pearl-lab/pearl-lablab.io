---
layout: page
title: Alumni
permalink: /alumni/
---

[Matthew Curtis-Maury](https://www.linkedin.com/in/matthew-curtis-maury-70409a20/), Ph.D., 2008    
[Filip Blagojevic](https://www.linkedin.com/in/filip-blagojevic-12312919/), Ph.D., 2008   
[Scott A. Schneider](https://www.scott-a-s.com/), Ph.D., 2009    
[Dong Li](https://faculty.ucmerced.edu/dong-li/) (also of [SCAPE](https://scape.cs.vt.edu/)) Ph.D., 2010  
[Mustafa Rafique](https://www.rit.edu/directory/mmrvcs-m-mustafa-rafique), (also of [DSSL](https://dssl.cs.vt.edu)) Ph.D., 2011  
[Jae-seung Yeom](https://people.llnl.gov/yeom2), Ph.D., 2014  
[Chun-Yi Su](https://www.linkedin.com/in/chunyisu/)(also of [SCAPE](https://scape.cs.vt.edu/), Ph.D., 2014  
[Alex Khasymski](https://www.linkedin.com/in/aleksandr-khasymski-15b1a819/) (also of [DSSL](https://dssl.cs.vt.edu/)) Ph.D., 2015  
Ankur Shah, M.Sc., 2008  
[Beran Nova Bryant](https://www.linkedin.com/in/beran-nova-bryant/), M.Sc., 2008  
[Harshil Shah](https://www.linkedin.com/in/harshil-shah-90427237/), M.Sc., 2008  
[Jyotirmaya Tripathi](https://www.linkedin.com/in/jyotirmayatripathi/), M.Sc., 2008  
[Benjamin Rose](https://www.linkedin.com/in/bar234/), M.Sc., 2009  
Daniel Moyer, M.Sc., 2023  
[Matthew Jackson](https://www.linkedin.com/in/matthew-jackson-a526541b7/), M.Sc., 2023  
[Melissa Cameron](https://www.linkedin.com/in/melissa-cameron-730a71b/), M.Sc., 2023  
[Shreya Bhandare](https://www.linkedin.com/in/shrebhan/), M.Sc., 2023  
[Manthan Shah](https://www.linkedin.com/in/nalostta/) M.Eng., 2023    
[Mostafa Kahla](https://www.linkedin.com/in/m-kahla/) M.Sc., 2023  
[Patric Fiaux](https://www.linkedin.com/in/patrickfiaux/), B.Sc, 2009  
[Lalitha Kuppa](https://www.linkedin.com/in/lkuppa/), B.Sc., 2022  
[Parker Harnack](https://www.linkedin.com/in/parker-harnack-070503195/), B.Sc., 2023  
[Angel Perez-Gonzalez](https://www.linkedin.com/in/ap2024/), B.Sc., 2024  
[Tatiana Monteiro](https://www.linkedin.com/in/ericatmonteiro2/), B.Sc., 2024  
[Riyos Pudasaini](https://www.linkedin.com/in/riyos-p-607157253/), B.Sc., 2024  
[Jeevan Shahi](https://www.linkedin.com/in/jeevan-shahi/) Undergraduate Researcher   
[James Whiting](https://www.linkedin.com/in/jamie-whiting/) Undergraduate Researcher   
