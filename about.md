---
layout: page
title: About
permalink: /about/
---

PEARL explores the performance and resilience of complex computing systems. Our research investigates how the hardware-software interface evolves with new technologies, applications, workloads, system complexity and scale.  Our research activity focuses on understanding and improving  the interfaces between runtime systems, operating systems, and emerging hardware. Our goal is to enable computing systems that are faster, more scalable, more resilient, and more sustainable. 
